@extends('layouts.mainLayout')
@section('title')
	Monitor SYTESA
@endsection
@section('styles')
	<link rel="stylesheet" href="{{url('lib/daterangepicker/daterangepicker.css')}}">
@endsection
@section('content')
	<div class="flex-row my-6">
		<div class="flex-row d-flex justify-content-between mt-5 px-2">
			<div class="btn-group shadow" role="group" aria-label="First group">
				<label class="input-group-text rounded-0" id="btnGroupAddon" for="range">
					Filtro
					<span class="add-on"><i class="far fa-calendar-alt" style="margin-left: 6px;"></i></span>
				</label>
				<input class="form-control rounded-0 text-muted"  type="text" name="range" id="range">
				<button type="button" class=" btn bg-rotoplas text-white" onclick="updateData(range='today')">Hoy</button>
				<button type="button" class=" btn bg-rotoplas text-white" onclick="updateData(range='yesterday')">Ayer</button>
				<button type="button" class=" btn bg-rotoplas text-white" onclick="updateData(range='week')">semana</button>
				<button type="button" class=" btn bg-rotoplas text-white" onclick="updateData(range='month')">mes</button>
			</div>
		</div>
	</div>
	<div class="row my-4 mx-0">
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC1" style=" height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Flujo de entrada</div>
							<p class="font-weight-bold" id="dataCard1"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tint fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC1" style=" height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text-xs font-weight-bold mb-1">Flujo de salida</div>
							<p class="font-weight-bold" id="dataCard2"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tint fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC6" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Temperatura</div>
							<p class="font-weight-bold" id="dataCard3"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-thermometer-quarter fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC2" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Estado de puerta</div>
							<p class="font-weight-bold" id="dataCard4"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-door-open fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC3" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Cárcamo de entrada</div>
							<p class="font-weight-bold" id="dataCard5"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tint fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC3" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Cárcamo de cloración</div>
							<p class="font-weight-bold" id="dataCard6"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tint fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4 mx-0">
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC3" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Cárcamo de envío</div>
							<p class="font-weight-bold" id="dataCard7"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tint fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC4" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Sensor de corriente 1</div>
							<p class="font-weight-bold" id="dataCard8"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tachometer-alt fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC4" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Sensor de corriente 2</div>
							<p class="font-weight-bold" id="dataCard9"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tachometer-alt fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC4" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Sensor de corriente 3</div>
							<p class="font-weight-bold" id="dataCard10"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tachometer-alt fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC5" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Sensor de voltaje 1</div>
							<p class="font-weight-bold" id="dataCard11"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tachometer-alt fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 col-lg-2 px-2 my-4 my-lg-0">
			<div class="card rounded-0 border-0 text-white shadow bgC5" style="height: 6rem;">
				<div class="row">
					<div class="col-9 ">
						<div class="card-body" style="line-height:1.2">
							<div class="text.xs font-weight-bold mb-1">Sensor de voltaje 2</div>
							<p class="font-weight-bold" id="dataCard12"></p>
						</div>
					</div>
					<div class="col-3 pl-0 py-4">
						<i class="fas fa-tachometer-alt fa-2x text-white"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Flujo de salida</h4>
					<h6 class="mb-1">Signet Paletas</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tint fcC1" style=""></i>
						<span id="dataChart1"></span>
					</p>
					<p class="card-content">
						<div id="chart1"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC1" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Flujo</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Acumulado </h4>
					<h1 id="dataCardChart1"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Temperatura</h4>
					<h6 class="mb-1">Ambiente</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-thermometer-quarter fcC6"></i>
						<span id="dataChart2"></span>
					</p>
					<p class="card-content">
						<div id="chart2"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC6" style="height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Temperatura</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Ultimo registro</h4>
					<h1 id="dataCardChart2"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Carcamo de entrada</h4>
					<h6 class="mb-1">Historico</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tint fcC3" style=""></i>
						<span id="dataChart3"></span>
					</p>
					<p class="card-content">
						<div id="chart3"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC3" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Carcamo de entrada</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Nivel</h4>
					<h1 id="dataCardChart3"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Carcamo de cloración</h4>
					<h6 class="mb-1">Historico</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tint fcC3" style=""></i>
						<span id="dataChart4"></span>
					</p>
					<p class="card-content">
						<div id="chart4"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC3" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Carcamo de cloración</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Nivel</h4>
					<h1 id="dataCardChart4"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Carcamo de envio</h4>
					<h6 class="mb-1">Historico</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tint fcC3" style=""></i>
						<span id="dataChart5"></span>
					</p>
					<p class="card-content">
						<div id="chart5"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC3" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Carcamo de envio</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Nivel</h4>
					<h1 id="dataCardChart5"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Puerta</h4>
					<h6 class="mb-1">Historico</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-door-closed fcC2" style=""></i>
						<span id="dataChart6"></span>
					</p>
					<p class="card-content">
						<div id="chart6"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC2" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Puerta</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Estado</h4>
					<h1 id="dataCardChart6"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Sensor de corriente 1</h4>
					<h6 class="mb-1">Bombeo de carcamo de cloracion a carcamo de envio</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tachometer-alt fcC4" style=""></i>
						<span id="dataChart7"></span>
					</p>
					<p class="card-content">
						<div id="chart7"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC4" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Consumo actual</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Ultimo dato</h4>
					<h1 id="dataCardChart7"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Sensor de corriente 2</h4>
					<h6 class="mb-1">Motor 1, entrada de agua al separador de grasas</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tachometer-alt fcC4" style=""></i>
						<span id="dataChart8"></span>
					</p>
					<p class="card-content">
						<div id="chart8"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC4" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Consumo actual</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Ultimo dato</h4>
					<h1 id="dataCardChart8"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Sensor de corriente 3</h4>
					<h6 class="mb-1">unidad de cloración</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tachometer-alt fcC4" style=""></i>
						<span id="dataChart9"></span>
					</p>
					<p class="card-content">
						<div id="chart9"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC4" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Consumo actual</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">ultimo dato</h4>
					<h1 id="dataCardChart9"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Sensor de voltaje 1</h4>
					<h6 class="mb-1">Voltaje alimentacion a bomba 120V</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tachometer-alt fcC5" style=""></i>
						<span id="dataChart10"></span>
					</p>
					<p class="card-content">
						<div id="chart10"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC5" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Consumo actual</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Ultimo dato</h4>
					<h1 id="dataCardChart10"></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Sensor de voltaje 2</h4>
					<h6 class="mb-1">Voltaje alimentacion a bomba 480V</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tachometer-alt fcC5" style=""></i>
						<span id="dataChart11"></span>
					</p>
					<p class="card-content">
						<div id="chart11"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC5" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Consumo actual</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Ultimo dato</h4>
					<h1 id="dataCardChart11"></h1>
				</div>
			</div>
		</div>
	</div>
	{{-- <div class="row my-4">
		<div class="col-12 col-md-8">
			<div class="card rounded-0 border-0 text-secondary shadow" style="background: #ffffff">
				<div class="card-body">
					<h4>Flujo de entrada</h4>
					<h6 class="mb-1">Title</h6>
					<p class="text-muted" style="font-size: 1.5em">
						<i class="fas fa-tint bgC1" style=""></i>
						<span id="dataChart12"></span>
					</p>
					<p class="card-content">
						<div id="chart12"></div>
					</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-4 p-5">
			<div class="card rounded-0 border-0 text-white shadow bgC1" style=" height: 100%">
				<div class="card-body">
					<h4 class="text.xs font-weight-bold">Title</h4>
					<h4 class="display-4 card-content mb-4" style="font-size: 2rem">Content</h4>
					<h1 id="dataCardChart12"></h1>
				</div>
			</div>
		</div>
	</div> --}}
@endsection
@section('scripts')
	<script src="{{url('lib/moment.min.js')}}"></script>
	<script src="{{url('lib/chartjs.min.js')}}"></script>
	<script src="{{url('lib/daterangepicker/daterangepicker.min.js')}}"></script>
	<script>
		$(document).ready(function () {
			var start = moment().startOf('day'),
				end = moment();
			var range = $('#range');
			range.val('Desde ' + start.format('MM/DD/YYYY') + ' hasta ' + end.format('MM/DD/YYYY'));

			range.daterangepicker({
				timePicker: true,
				startDate:start,
				endDate:end,
				maxDate:moment(),
				locale: {
					format: 'M/DD hh:mm A'
				},
				ranges: {
					'Ultimos 30 días': [moment().subtract(29, 'days').startOf('day'), moment()],
					'Ultimos 7 días': [moment().subtract(6, 'days').startOf('day'), moment()],
					'Ayer': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
					'Hoy': [moment().startOf('day'), moment()],
					'Ultimas 5 horas': [moment().subtract(5, 'hour'), moment()],
					'Ultimas 2 horas': [moment().subtract(2, 'hour'), moment()],
					'Ultima hora': [moment().subtract(1, 'hour'), moment()],
				},
			}, function (start, end) {
				updateData(null,start.format('YYYY-MM-DD HH:mm:ss'),end.format('YYYY-MM-DD HH:mm:ss'));
			});
			start = moment().startOf('day').format('YYYY-MM-DD HH:mm:ss');
			end = moment().format('YYYY-MM-DD HH:mm:ss');
			updateData(null,start,end);
		});
		function updateData(range=null,start,end){
			if(range!=null){
				switch(range){
					case 'today':
						start=moment().startOf('day').format('YYYY-MM-DD HH:mm:ss');
						end=moment().format('YYYY-MM-DD HH:mm:ss');
						$('#range').val(moment(start).format('M/DD HH:mm')+' - '+moment(end).format('M/DD HH:mm'));
						break;
					case 'yesterday':
						start=moment().subtract(1, 'days').startOf('day').format('YYYY-MM-DD HH:mm:ss');
						end=moment().subtract(1, 'days').endOf('day').format('YYYY-MM-DD HH:mm:ss');
						$('#range').val(moment(start).format('M/DD HH:mm')+' - '+moment(end).format('M/DD HH:mm'));
						break;
					case 'month':
						start=moment().startOf('month').format('YYYY-MM-DD HH:mm:ss');
						end=moment().endOf('month').format('YYYY-MM-DD HH:mm:ss');
						$('#range').val(moment(start).format('M/DD HH:mm')+' - '+moment(end).format('M/DD HH:mm'));
						break;
					case 'week':
						start=moment().startOf('week').format('YYYY-MM-DD HH:mm:ss');
						end=moment().endOf('week').format('YYYY-MM-DD HH:mm:ss');
						$('#range').val(moment(start).format('M/DD HH:mm')+' - '+moment(end).format('M/DD HH:mm'));
						break;
				}
			}
			$('#chart1').html('<canvas id="cChart1" style="height:300px"></canvas>')
			$('#chart2').html('<canvas id="cChart2" style="height:300px"></canvas>')
			$('#chart3').html('<canvas id="cChart3" style="height:300px"></canvas>')
			$('#chart4').html('<canvas id="cChart4" style="height:300px"></canvas>')
			$('#chart5').html('<canvas id="cChart5" style="height:300px"></canvas>')
			$('#chart6').html('<canvas id="cChart6" style="height:300px"></canvas>')
			$('#chart7').html('<canvas id="cChart7" style="height:300px"></canvas>')
			$('#chart8').html('<canvas id="cChart8" style="height:300px"></canvas>')
			$('#chart9').html('<canvas id="cChart9" style="height:300px"></canvas>')
			$('#chart10').html('<canvas id="cChart10" style="height:300px"></canvas>')
			$('#chart11').html('<canvas id="cChart11" style="height:300px"></canvas>')
			$('#chart12').html('<canvas id="cChart12" style="height:300px"></canvas>')
			$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
			$.ajax({
				url:'api/sensor/data',
				data:{
					'from':start,
					'to':end,
					'sM2_minSignet':0.01,
					'toJson':true,
				},
				success:function(response){
					//collecting data
					console.log(response);
					data=JSON.parse(response);
					sumSignetMega2=0;
					outFlow=[];
					outFlowTime=[];
					temperature=[];
					temperatureTime=[];
					cIn=[];
					cInTime=[];
					cC=[];
					cCTime=[];
					cOut=[];
					cOutTime=[];
					gate=[];
					gateTime=[];
					current1=[];
					current1Time=[];
					current2=[];
					current2Time=[];
					current3=[];
					current3Time=[];
					volt1=[];
					volt1Time=[];
					volt2=[];
					volt2Time=[];
					$.each(data['mini1'],function(i,row){
						current1[i]=row['current1'];
						current1Time[i]=moment(row['datetime']).format('MM-DD HH:mm');
						current2[i]=row['current2'];
						current2Time[i]=moment(row['datetime']).format('MM-DD HH:mm');
						current3[i]=row['current3'];
						current3Time[i]=moment(row['datetime']).format('MM-DD HH:mm');
						volt1[i]=row['volt1'];
						volt1Time[i]=moment(row['datetime']).format('MM-DD HH:mm');
						volt2[i]=row['volt2'];
						volt2Time[i]=moment(row['datetime']).format('MM-DD HH:mm');
					});
					$.each(data['mega1'],function(i,row){
						temperature[i]=row['temperature'];
						temperatureTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
						gate[i]=row['gate'];
						gateTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
					});
					$.each(data['mega2'],function(i,row){
						sumSignetMega2+=row['signet'];
						outFlow[i]=row['signet'];
						outFlowTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
						cIn[i]=row['cIn'];
						cInTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
						cC[i]=row['cC'];
						cCTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
						cOut[i]=row['cOut'];
						cOutTime[i]=moment(row['datetime']).format('MM-DD HH:mm');
					});
					
					//updating cards
					$('#dataCard2').html(sumSignetMega2.toFixed(2)+'L');
					$('#dataCard3').html(data['mega1'][0]['temperature']+'°C');
					$('#dataCard4').html(gateState[data['mega1'][0]['gate']]);
					$('#dataCard5').html('Nivel '+level[data['mega2'][0]['cIn']]);
					$('#dataCard6').html('Nivel '+level[data['mega2'][0]['cC']]);
					$('#dataCard7').html('Nivel '+level[data['mega2'][0]['cOut']]);
					$('#dataCard8').html(data['mini1'][0]['current1']+'A');
					$('#dataCard9').html(data['mini1'][0]['current2']+'A');
					$('#dataCard10').html(data['mini1'][0]['current3']+'A');
					$('#dataCard11').html(data['mini1'][0]['volt1']+'V');
					$('#dataCard12').html(data['mini1'][0]['volt2']+'V');
					if(data['mega1'][0]['gate']==1)$('.fa-door-closed').removeClass('fa-door-closed').addClass('fa-door-open');
					if(data['mega1'][0]['gate']==0)$('.fa-door-open').removeClass('fa-door-open').addClass('fa-door-closed');
					//updating charts
					//chart1
					$('#dataChart1').html(outFlow[0]+'L');
					$('#dataCardChart1').html(sumSignetMega2.toFixed(2)+'L');
					canv1=$('#cChart1');
					var canv1 = cChart1.getContext("2d");
					var gradientStroke1 = canv1.createLinearGradient(0, 230, 0, 50);
					gradientStroke1.addColorStop(1, selectColor('c1',.4));
					gradientStroke1.addColorStop(.5, selectColor('c1',.5));
					gradientStroke1.addColorStop(.2, selectColor('c1',.9));
					gradientStroke1.addColorStop(0, selectColor('c1',1));
					drawChart($('#cChart1'), outFlowTime.reverse(), outFlow.reverse(), 'Lts', gradientStroke1, type = 'bar');
					//chart2
					$('#dataChart2').html(temperature[0]+'°C');
					$('#dataCardChart2').html(temperature[0]+'°C'+'<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+temperatureTime[0])+'</span>';
					canv2=$('#cChart2');
					var canv2 = cChart2.getContext("2d");
					var gradientStroke2 = canv2.createLinearGradient(0, 230, 0, 50);
					gradientStroke2.addColorStop(1, selectColor('c6',.4));
					gradientStroke2.addColorStop(.5, selectColor('c6',.5));
					gradientStroke2.addColorStop(.2, selectColor('c6',.9));
					gradientStroke2.addColorStop(0, selectColor('c6',1));
					drawChart($('#cChart2'), temperatureTime.reverse(), temperature.reverse(), '°C', gradientStroke2, type = 'line');
					//chart3
					$('#dataChart3').html('Nivel '+level[cIn[0]]);
					$('#dataCardChart3').html(level[cIn[0]]+'<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+cInTime[0])+'</span>';
					canv3=$('#cChart3');
					var canv3 = cChart3.getContext("2d");
					var gradientStroke3 = canv3.createLinearGradient(0, 230, 0, 50);
					gradientStroke3.addColorStop(1, selectColor('c3',.4));
					gradientStroke3.addColorStop(.5, selectColor('c3',.5));
					gradientStroke3.addColorStop(.2, selectColor('c3',.9));
					gradientStroke3.addColorStop(0, selectColor('c3',1));
					drawChart($('#cChart3'), cInTime.reverse(), cIn.reverse(), 'Level', gradientStroke3, type = 'line');
					//chart4
					$('#dataChart4').html('Nivel '+level[cC[0]]);
					$('#dataCardChart4').html(level[cC[0]]+'<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+cCTime[0])+'</span>';
					canv4=$('#cChart4');
					var canv4 = cChart4.getContext("2d");
					var gradientStroke4 = canv4.createLinearGradient(0, 230, 0, 50);
					gradientStroke4.addColorStop(1, selectColor('c3',.4));
					gradientStroke4.addColorStop(.5, selectColor('c3',.5));
					gradientStroke4.addColorStop(.2, selectColor('c3',.9));
					gradientStroke4.addColorStop(0, selectColor('c3',1));
					drawChart($('#cChart4'), cCTime.reverse(), cC.reverse(), 'Level', gradientStroke4, type = 'line');
					//chart5
					$('#dataChart5').html('Nivel '+level[cOut[0]]);
					$('#dataCardChart5').html(level[cOut[0]]+'<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+cOutTime[0])+'</span>';
					canv5=$('#cChart5');
					var canv5 = cChart5.getContext("2d");
					var gradientStroke5 = canv5.createLinearGradient(0, 230, 0, 50);
					gradientStroke5.addColorStop(1, selectColor('c3',.4));
					gradientStroke5.addColorStop(.5, selectColor('c3',.5));
					gradientStroke5.addColorStop(.2, selectColor('c3',.9));
					gradientStroke5.addColorStop(0, selectColor('c3',1));
					drawChart($('#cChart5'), cOutTime.reverse(), cOut.reverse(), 'Level', gradientStroke5, type = 'line');
					//chart6
					$('#dataChart6').html(gateState[gate[0]]);
					$('#dataCardChart6').html(gateState[gate[0]]+'<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+gateTime[0])+'</span>';
					canv6=$('#cChart6');
					var canv6 = cChart6.getContext("2d");
					var gradientStroke6 = canv6.createLinearGradient(0, 230, 0, 50);
					gradientStroke6.addColorStop(1, selectColor('c2',.4));
					gradientStroke6.addColorStop(.5, selectColor('c2',.5));
					gradientStroke6.addColorStop(.2, selectColor('c2',.9));
					gradientStroke6.addColorStop(0, selectColor('c2',1));
					drawChart($('#cChart6'), gateTime.reverse(), gate.reverse(), 'Estado', gradientStroke6, type = 'line');
					//chart7
					$('#dataChart7').html(current1[0]);
					$('#dataCardChart7').html(current1[0]+'A<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+current1Time[0])+'</span>';
					canv7=$('#cChart7');
					var canv7 = cChart7.getContext("2d");
					var gradientStroke7 = canv7.createLinearGradient(0, 230, 0, 50);
					gradientStroke7.addColorStop(1, selectColor('c4',.4));
					gradientStroke7.addColorStop(.5, selectColor('c4',.5));
					gradientStroke7.addColorStop(.2, selectColor('c4',.9));
					gradientStroke7.addColorStop(0, selectColor('c4',1));
					drawChart($('#cChart7'), current1Time.reverse(), current1.reverse(), 'Estado', gradientStroke7, type = 'line');
					//chart8
					$('#dataChart8').html(current2[0]);
					$('#dataCardChart8').html(current2[0]+'A<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+current2Time[0])+'</span>';
					canv8=$('#cChart8');
					var canv8 = cChart8.getContext("2d");
					var gradientStroke8 = canv8.createLinearGradient(0, 230, 0, 50);
					gradientStroke8.addColorStop(1, selectColor('c4',.4));
					gradientStroke8.addColorStop(.5, selectColor('c4',.5));
					gradientStroke8.addColorStop(.2, selectColor('c4',.9));
					gradientStroke8.addColorStop(0, selectColor('c4',1));
					drawChart($('#cChart8'), current2Time.reverse(), current2.reverse(), 'Estado', gradientStroke8, type = 'line');
					//chart9
					$('#dataChart9').html(current3[0]);
					$('#dataCardChart9').html(current3[0]+'A<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+current3Time[0])+'</span>';
					canv9=$('#cChart9');
					var canv9 = cChart9.getContext("2d");
					var gradientStroke9 = canv9.createLinearGradient(0, 230, 0, 50);
					gradientStroke9.addColorStop(1, selectColor('c4',.4));
					gradientStroke9.addColorStop(.5, selectColor('c4',.5));
					gradientStroke9.addColorStop(.2, selectColor('c4',.9));
					gradientStroke9.addColorStop(0, selectColor('c4',1));
					drawChart($('#cChart9'), current3Time.reverse(), current3.reverse(), 'Estado', gradientStroke9, type = 'line');
					//chart10
					$('#dataChart10').html(volt1[0]);
					$('#dataCardChart10').html(volt1[0]);
					$('#dataCardChart10').html(volt1[0]+'V<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+volt1Time[0])+'</span>';					
					canv10=$('#cChart10');
					var canv10 = cChart10.getContext("2d");
					var gradientStroke10 = canv10.createLinearGradient(0, 230, 0, 50);
					gradientStroke10.addColorStop(1, selectColor('c5',.4));
					gradientStroke10.addColorStop(.5, selectColor('c5',.5));
					gradientStroke10.addColorStop(.2, selectColor('c5',.9));
					gradientStroke10.addColorStop(0, selectColor('c5',1));
					drawChart($('#cChart10'), volt1Time.reverse(), volt1.reverse(), 'Estado', gradientStroke10, type = 'line');
					//chart11
					$('#dataChart11').html(volt2[0]);
					$('#dataCardChart11').html(volt2[0]+'V<br><span class="display-4" style="font-size:1.5rem">Tiempo de registro:<br> '+volt2Time[0])+'</span>';					
					canv11=$('#cChart11');
					var canv11 = cChart11.getContext("2d");
					var gradientStroke11 = canv11.createLinearGradient(0, 230, 0, 50);
					gradientStroke11.addColorStop(1, selectColor('c5',.4));
					gradientStroke11.addColorStop(.5, selectColor('c5',.5));
					gradientStroke11.addColorStop(.2, selectColor('c5',.9));
					gradientStroke11.addColorStop(0, selectColor('c5',1));
					drawChart($('#cChart11'), volt2Time.reverse(), volt2.reverse(), 'Estado', gradientStroke11, type = 'line');
				}
			});
		};
		level={
			1:'bajo',
			2:'medio',
			3:'alto',
		};
		gateState={
			0:'cerrado',
			1:'abierto',
		}

		function drawChart(canvas, labels, dataX, label, color,type, displayAxes=true,displayLegend=false) {
			var ctx = canvas;
			var myChart = new Chart(
				ctx, {type: type,responsive: true,legend: {display: false},
				data: {
					labels: labels,
					datasets: [{
						label: label,
						fill: true,
						backgroundColor: color,
						hoverBackgroundColor: color,
						//borderColor: '#1f8ef1',
						borderWidth: 1,
						borderDash: [],
						borderDashOffset: 0.0,
						data: dataX
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {display: displayLegend},
					tooltips: {
						backgroundColor: '#f5f5f5',
						titleFontColor: '#333',
						bodyFontColor: '#666',
						bodySpacing: 4,
						xPadding: 12,
						mode: "nearest",
						intersect: 0,
						position: "nearest"
					},
					responsive: true,
					scales: {
						yAxes: [
							{barPercentage: 1,display:displayAxes,gridLines: {drawBorder: false,color: 'rgba(29,140,248,0.1)',zeroLineColor: "transparent",},
								ticks: {suggestedMin: 0.1,suggestedMax: 0.15,padding: 20,fontColor: "#9a9a9a"},
								scaleLabel: {display: true,labelString: label,fontSize: 20}
						}],
						xAxes: [
							{barThickness:5,  barPercentage: 1.00,display:displayAxes,gridLines: {drawBorder: false,color: 'rgba(29,140,248,0.1)',zeroLineColor: "transparent",},
								ticks: {padding: 20,fontColor: "#9a9a9a"},
								scaleLabel: {display: false,labelString: label,fontSize: 20}
						}]
					}
				},
			});
			//console.log(myChart);
			
		}
		function selectColor(color,opacity=1){
			switch(color){
				case 'c1':return 'rgb(0, 214, 180, '+opacity+')';
				case 'c2':return 'rgb(0, 99, 214, '+opacity+')';
				case 'c3':return 'rgb(121, 170, 210, '+opacity+')';
				case 'c4':return 'rgb(255, 154, 2, '+opacity+')';
				case 'c5':return 'rgb(170, 72, 215, '+opacity+')';
				case 'c6':return 'rgb(240, 90, 56, '+opacity+')';
			}
		}

	</script>
@endsection