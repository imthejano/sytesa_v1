<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SensorMega1;
use App\SensorMega2;
use App\SensorMini1;
use DB;
use Carbon\Carbon;


class SensorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get(Request $request){
        $from=$request->input('from');
		$to=$request->input('to');
        
		$sM1_minSignet=$request->input('sM1_minSignet',0);
        $sM2_minSignet=$request->input('sM2_minSignet',0);

        $sM1_minTemperature=$request->input('sM1_minTemperature',0);
        
		$sM1_gate=$request->input('sM1_gate');
        
        $sensorMini1=$request->input('sensorMini1',true);
        $sensorMega1=$request->input('sensorMega1',true);
        $sensorMega2=$request->input('sensorMega2',true);

        $toJson=$request->input('toJson',false);
        if($sensorMini1){
            $mini1=SensorMini1::select(DB::raw('device, date(timefox) as date, month(timefox) as month, year(timefox) as year, time(timefox) as time, timefox as datetime, FORMAT(corrienteuno,2) as current1,FORMAT(corrientedos,2) as current2,FORMAT(corrientetres,2) as current3, FORMAT(voltajeuno,2) as volt1, FORMAT(voltajedos,2) as volt2'));
            if($from!=null)$mini1=$mini1->where('timefox','>=',$from);
            if($to!=null)$mini1=$mini1->where('timefox','<=',$to);
            $mini1=$mini1->orderBy('timefox','desc');
            $mini1=$mini1->get();
            $response['mini1']=$mini1;
        }
        if($sensorMega1){
            $mega1=SensorMega1::select(DB::raw('device, date(timefox) as date, month(timefox) as month, year(timefox) as year, time(timefox) as time, timefox as datetime, signet, temperatura as temperature, switch as gate'));
            $mega1=$mega1->where('signet','>=',$sM1_minSignet);
            $mega1=$mega1->where('temperatura','>=',$sM1_minTemperature);
            if($sM1_gate!=null)$mega1=$mega1->where('gate','=',$sM1_gate);
            if($from!=null)$mega1=$mega1->where('timefox','>=',$from);
            if($to!=null)$mega1=$mega1->where('timefox','<=',$to);
            $mega1=$mega1->orderBy('timefox','desc');
            $mega1=$mega1->get();
            $response['mega1']=$mega1;
        }
        if($sensorMega2){
            $mega2=SensorMega2::select(DB::raw('device, date(timefox) as date, month(timefox) as month, year(timefox) as year, time(timefox) as time, timefox as datetime, nivel_cb as cIn, signet, clorador as cC, filtro as cOut'));
            $mega2=$mega2->where('signet','>=',$sM2_minSignet);
            if($from!=null)$mega2=$mega2->where('timefox','>=',$from);
            if($to!=null)$mega2=$mega2->where('timefox','<=',$to);
            $mega2=$mega2->orderBy('timefox','desc');
            $mega2=$mega2->get();
            $response['mega2']=$mega2;
        }
        
		if($toJson)return json_encode($response);
		else return $response;
    }
}