<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorMini1 extends Model
{
    protected $table = 'sensor_mini_uno';    
}
