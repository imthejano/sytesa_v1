<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorMega1 extends Model
{
    protected $table = 'sensor_mega_uno';
}
